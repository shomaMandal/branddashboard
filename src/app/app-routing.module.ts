import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {AuthguardGuard} from './authguard.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
const routes: Routes = [{
  path: '',
  component: LoginComponent
},
{
  path: 'login',
  component: LoginComponent
},
{
  path: ':lang/login',
  component: LoginComponent
},
{
  path: ':lang/dashboard/:brandName',
  canActivate : [ AuthguardGuard ],
  loadChildren: './dashboard/dashboard.module#DashboardModule',
  component: DashboardComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ServicesService } from './services.service';
import {Config} from '../config';

@Injectable({
  providedIn: 'root'
})
export class AuthguardGuard implements CanActivate {
  constructor( public user: ServicesService, public router: Router, public conf: Config){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if ( !this.user.getLogin() ) {
        // this.router.navigate([{outlets: {main: 'login'}}]);
        this.conf.lang = localStorage.getItem('lang');
        // this.router.navigate([this.conf.lang + '/login']);
        this.router.navigate(['en/login']);
      }
      return this.user.getLogin();
  }
}

// var backendUrl = 'http://192.168.1.102:8080/';
var backendUrl = 'http://106.14.164.125:8083/';
// var backendUrl = 'http://47.100.219.37:8081/services/pjdarenapi/';
export const environment = {
  production: false,
  backendUrl: backendUrl,
  imgURL: "http://pjdaren.oss-cn-shanghai.aliyuncs.com/wom/prod/",
  exportUrl: backendUrl
};
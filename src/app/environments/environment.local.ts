var backendUrl = 'http://localhost:8080/';
export const environment = {
  production: false,
  backendUrl: backendUrl,
  imgURL: backendUrl + "api/v1/image/",
  exportUrl: backendUrl
};

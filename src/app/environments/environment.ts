// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
//var backendUrl = 'http://106.14.164.125:8082/';
var backendUrl = 'http://localhost:8080/';
export const environment = {
  production: false,
  backendUrl: backendUrl,
  imgURL: backendUrl + "api/v1/image/",
  exportUrl: backendUrl
};

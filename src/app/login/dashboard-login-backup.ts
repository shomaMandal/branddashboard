import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Config} from '../../config';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  environment: any;
  doLogin = true;
  loginUser = {
    username : '',
    password : '',
    authToken : '',
    login: '',
    userId: '',
    name: '',
    nickname: '',
    profileImage: '',
    authorities: ''
  };

  form: FormGroup;
  signupForm: FormGroup;

  public isAdmin = false;
  public checkAuthorities = [
    'ROLE_ADMIN',
    'ROLE_BRAND'
  ];

  public formSubmitAttempt: boolean;

  @ViewChild('image', {static: false}) image: ElementRef;
  constructor(
    public conf: Config,
    public http: HttpClient,
    public user: ServicesService,
    public router: Router,
    public formBuilder: FormBuilder
  ) {
    this.environment = conf.environment;
    this.form = this.formBuilder.group({
      username: new FormControl('',
        Validators.compose([
          Validators.required,
          Validators.minLength(10),
        ]),
      ),
      password: new FormControl('',
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
        ]),
      ),
      remember: new FormControl('')
    });
    this.signupForm = this.formBuilder.group({
      imageName: new FormControl(''),
      imageFileName: new FormControl(''),
      name: new FormControl('',
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
        ]),
      ),
      emailAddress: new FormControl('',
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
        ]),
      ),
      phoneNumber: new FormControl('',
        Validators.compose([
          Validators.required,
          Validators.minLength(10),
        ]),
      ),
      brandName: new FormControl('',
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
        ]),
      ),
      newpassword: new FormControl('',
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
        ]),
      ),
      confirmpassword: new FormControl('',
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
        ]),
      )
    });
  }

  ngOnInit() {
    if ( this.conf.currRoute() === '/' ) {
      this.conf.navigate([this.conf.lang + '/login']);
    } else if ( this.user.getLogin() ) {
      this.conf.navigate([this.conf.lang + '/dashboard/badges']);
    } else {
      this.form = this.formBuilder.group({
        username: new FormControl('',
          Validators.compose([
            Validators.required,
            Validators.minLength(10),
          ]),
        ),
        password: new FormControl('',
          Validators.compose([
            Validators.required,
            Validators.minLength(8),
          ]),
        ),
        remember: new FormControl(false)
      });
    }
  }

  actionLoginUser ( loginUser ) {
    this.conf.loader(1);
    var _self = this;
    let passData = {
      username: loginUser.username,
      password: loginUser.password,
      rememberMe: loginUser.remember
    }
    this.conf.postApiCall( 'users/login_cpanel', passData, function( res ){
      // this.conf.postApiCall( 'users/login_cpanel', passData, function( res ){
      if ( res.token != undefined && res.user.authorities !== undefined ) {
        for (var i = _self.checkAuthorities.length - 1; i >= 0; i--) {
          if ( res.user.authorities.indexOf( _self.checkAuthorities[i] ) > -1 ) {
            _self.isAdmin = true;
          }
        }
        if ( _self.isAdmin ) {
          loginUser.authToken = res.token;
          if ( res.user.login != undefined ) {
            loginUser.login = res.user.login;
          }
          if ( res.user.name != undefined ) {
            loginUser.name = res.user.name;
          }
          if ( res.user.profileImage != undefined ) {
            loginUser.profileImage = res.user.profileImage;
          }
          if ( res.user.authorities != undefined ) {
            loginUser.authorities = res.user.authorities;
          }
          if ( res.user.id != undefined ) {
            loginUser.userId = res.user.id;
          }
          _self.user.setLogin( loginUser );
          localStorage.setItem( 'remember', _self.form.value.remember);
          _self.conf.toast('You have successfully logged in!', '', 'info' );
          _self.conf.loader(2);
        } else {
          _self.conf.toast('You do not have Administrative Rights.', '5000', 'warning' );
          _self.conf.loader(2);
        }
      }
    },
    function( err ) {
      if ( err.detail !== undefined ) {
        _self.conf.toast(err.detail, '', 'error' );
      } else {
        _self.conf.toast('', '', 'error' );
      }
    });
    return false;
  }
  actionsignupUser ( loginUser ) {
    var _self = this;
    let passData = {
      profileImage: loginUser.imageFileName,
      name: loginUser.name,
      brandnName: loginUser.brandnName,
      phoneNumber: loginUser.phoneNumber,
      email: loginUser.emailAddress,
      oldpassword: loginUser.oldpassword,
      newpassword: loginUser.newpassword,
      confirmpassword: loginUser.confirmpassword
    }
    this.conf.postApiCall( 'users', passData, function( res ) {
      // if ( res.token != undefined && res.user.authorities !== undefined ) {
      //   for (var i = _self.checkAuthorities.length - 1; i >= 0; i--) {
      //     if ( res.user.authorities.indexOf( _self.checkAuthorities[i] ) > -1 ) {
      //       _self.isAdmin = true;
      //     }
      //   }
      //   if ( _self.isAdmin ) {
      //     loginUser.authToken = res.token;
      //     if ( res.user.login != undefined ) {
      //       loginUser.login = res.user.login;
      //     }
      //     if ( res.user.name != undefined ) {
      //       loginUser.name = res.user.name;
      //     }
      //     if ( res.user.nickname != undefined ) {
      //       loginUser.nickname = res.user.nickname;
      //     }
      //     if ( res.user.profileImage != undefined ) {
      //       loginUser.profileImage = res.user.profileImage;
      //     }
      //     if ( res.user.authorities != undefined ) {
      //       loginUser.authorities = res.user.authorities;
      //     }
      //     if ( res.user.id != undefined ) {
      //       loginUser.userId = res.user.id;
      //     }
      //     _self.user.setLogin( loginUser );
      //   } else {
      //     _self.conf.tost( 0, 'You do not have Administrative Rights.' );
      //   }
      // }
    },
    function( err ) {
      if ( err.detail !== undefined ) {
        _self.conf.toast(err.detail, '', 'error' );
      } else {
        _self.conf.toast('', '', 'error' );
      }
    });
    return false;
  }
  isFieldValid(field: string) {
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.formSubmitAttempt)
    );
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }
  uploadImage() {
    const file = this.image.nativeElement['files'][0];
    const reader = new FileReader();
    reader.addEventListener('loadend', () => {
      this.signupForm.patchValue({imageName: reader.result});
      this.signupForm.patchValue({imageFileName: file.name});
    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }
}

import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import {findIndex} from 'lodash';
import {Router} from '@angular/router';
import {Config} from '../config';
@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  public isLogin;
  public username;
  public renderer: Renderer2
  constructor(
    public router: Router, rendererFactory: RendererFactory2
  ) {
    this.renderer = rendererFactory.createRenderer(null, null);
    this.isLogin = false;
    this.username = 'Guest';

    if ( localStorage.getItem( 'isLogin' ) == 'true' ) {
      this.isLogin = true;
    }

    if ( localStorage.getItem( 'username' ) ) {
      this.username = localStorage.getItem( 'username' );
    }
  }
  makeLogout() {
    localStorage.removeItem( 'isLogin' );
    localStorage.removeItem( 'userId' );
    localStorage.removeItem( 'username' );
    localStorage.removeItem( 'authToken' );
    localStorage.removeItem( 'login' );
    localStorage.removeItem( 'name' );
    localStorage.removeItem( 'nickname' );
    localStorage.removeItem( 'profileImage' );
    localStorage.removeItem( 'authorities' );
    //localStorage.clear();
    this.isLogin = false;
    this.router.navigate(['/login']);
  }

  setLogin( loginUser ) {
    this.router.navigate([localStorage.getItem('lang') + '/dashboard/campaign']);
    localStorage.setItem( 'isLogin', 'true' );
    if ( loginUser.login !== undefined ) {
      localStorage.setItem( 'username', loginUser.login );
    } else {
      localStorage.setItem( 'username', '' );
    }
    if ( loginUser.login !== undefined ) {
      localStorage.setItem( 'login', loginUser.login );
    } else {
      localStorage.setItem( 'login', '' );
    }
    if ( loginUser.userId !== undefined ) {
      localStorage.setItem( 'userId', loginUser.userId );
    } else {
      localStorage.setItem( 'userId', '' );
    }
    if ( loginUser.name !== undefined ) {
      localStorage.setItem( 'name', loginUser.name );
    } else {
      localStorage.setItem( 'name', '' );
    }
    if ( loginUser.nickname !== undefined ) {
      localStorage.setItem( 'nickname', loginUser.nickname );
    } else {
      localStorage.setItem( 'nickname', '' );
    }
    if ( loginUser.profileImage !== undefined ) {
      localStorage.setItem( 'profileImage', loginUser.profileImage );
    } else {
      localStorage.setItem( 'profileImage', '' );
    }
    if ( loginUser.authorities !== undefined ) {
      localStorage.setItem( 'authorities', loginUser.authorities );
    } else {
      localStorage.setItem( 'authorities', '' );
    }
    this.isLogin = true;
    this.router.navigate([localStorage.getItem('lang') + '/dashboard/' + loginUser.brandName + '/badges']);
  }

  getLogin() {
    if ( localStorage.getItem( 'isLogin' ) == 'true' ) {
      this.isLogin = true;
    } else {
      this.isLogin = false;
    }
    return this.isLogin;
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { AuthguardGuard } from '../authguard.guard';
import { CampaignPageComponent } from './campaign-page/campaign-page.component';
import { MenuComponent } from './campaign-page/menu/menu.component';

const routes: Routes = [{
  path: 'profile',
  canActivate : [ AuthguardGuard ],
  component: ProfilePageComponent
}, {
  path: 'badges',
  canActivate : [ AuthguardGuard ],
  component: CampaignPageComponent
}
// , {
//   path: 'badges/:id1/campaign/:id2',
//   canActivate : [ AuthguardGuard ],
//   loadChildren: './campaign-page/menu/menu.module#MenuModule',
//   component: MenuComponent
// }
, {
  path: 'badges/:id1/campaign/:id2',
  canActivate : [ AuthguardGuard ],
  loadChildren: './campaign-page/menu/menu.module#MenuModule',
  component: MenuComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }

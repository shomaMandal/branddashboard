import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Config} from '../../config';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  isLogin = false;
  profileImage = '';
  brandName = '';
  constructor(public conf: Config,
    public route: ActivatedRoute,
    public router: Router) {
      this.route.params.subscribe( params => {
        this.conf.translate.setDefaultLang(params['lang']);
        this.conf.translate.use(params['lang']);
        this.conf.navigate([this.conf.lang + '/dashboard/' + localStorage.getItem('brandName')]);
      });
  }
  ngOnInit() {
    this.profileImage = this.conf.putImage(localStorage.getItem('profileImage'));
  }
  redirectBadge() {
    this.conf.navigate([this.conf.lang + '/dashboard/' + localStorage.getItem('brandName') + '/badges']);
  }
  redirectProfile() {
    this.conf.navigate([this.conf.lang + '/dashboard/' + localStorage.getItem('brandName') + '/profile']);
  }
  logout() {
    localStorage.removeItem( 'isLogin' );
    localStorage.removeItem( 'userId' );
    localStorage.removeItem( 'username' );
    localStorage.removeItem( 'authToken' );
    localStorage.removeItem( 'login' );
    localStorage.removeItem( 'name' );
    localStorage.removeItem( 'nickname' );
    localStorage.removeItem( 'profileImage' );
    localStorage.removeItem( 'authorities' );
    localStorage.removeItem( 'brandName' );
    // localStorage.clear();
    this.isLogin = false;
    this.conf.navigate([this.conf.lang + '/login']);
  }
}

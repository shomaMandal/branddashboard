import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray} from '@angular/forms';
import { Config } from 'src/config';
import { HttpClient } from '@angular/common/http';
import { ServicesService } from 'src/app/services.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {
  authorities=[];
  hideForm = true;
  profileForm: FormGroup;
  controls: any = [];
  brandName;
  profileImage = '';
  userId = '';
  @ViewChild('image', {static: false}) image: ElementRef;
  constructor(public conf: Config,
    public http: HttpClient,
    public translate: TranslateService,
    public user: ServicesService,
    public formBuilder: FormBuilder,
    public route: ActivatedRoute) {
      this.brandName = localStorage.getItem('brandName');
      this.conf.translate.setDefaultLang('nl');
      this.conf.translate.use('nl');
      this.conf.navigate([localStorage.getItem('lang') + '/dashboard/'  + localStorage.getItem('brandName') + '/profile']);
      this.profileForm = this.formBuilder.group({
      imageName: new FormControl(''),
      imageFileName: new FormControl(''),
      name: new FormControl('', Validators.compose([
          Validators.required,
          Validators.minLength(2),
        ]),
      ),
      email: new FormControl('', Validators.compose([
          Validators.required,
          Validators.minLength(2),
        ]),
      ),
      phoneNumber: new FormControl('', Validators.compose([
          Validators.required,
          Validators.minLength(10),
        ]),
      ),
      userList: this.formBuilder.array([]),
      oldpassword: new FormControl('',
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
        ]),
      ),
      newpassword: new FormControl('',
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
        ]),
      ),
      confirmpassword: new FormControl('',
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
        ]),
      )
      });
      this.controls = this.profileForm.controls['userList'];
    // this.addUsers();
  }
  // addUsers() {
  //   const newGroup = this.formBuilder.group({
  //     name: new FormControl(''),
  //     phoneNumber: new FormControl('')
  //   });
  //   this.controls.push(newGroup);
  //   this.controls = <FormArray> this.controls;
  // }
  ngOnInit() {
    this.userId = localStorage.getItem('userId');
    let _self = this;
    this.conf.getApiCall( 'users/account', {}, function( res ) {
      // console.log(res);
      if (res.profileImage) {
        localStorage.setItem('profileImage', res.profileImage);
        _self.profileImage = res.profileImage;
        _self.profileForm.patchValue({imageName: res.profileImage});
      }
      if (res.name) {
        _self.profileForm.patchValue({name: res.name});
      }
      if (res.email) {
        _self.profileForm.patchValue({email: res.email});
      }
      if (res.phoneNumber) {
        _self.profileForm.patchValue({phoneNumber: res.phoneNumber});
      }
      if (res.authorities) {
        for (let index = 0; index < res.authorities.length; index++) {
          _self.authorities.push(res.authorities[index]);
        }
      }
    },
    function( err ) {
      if ( err.detail != undefined ) {
        _self.conf.toast(err.detail, '', 'error' );
      } else {
        _self.conf.toast('', '', 'error' );
      }
    });
  }
  updateUserAccount() {
    this.conf.loader(1);
    let _self = this;
    let data = {
      phoneNumber: _self.profileForm.value.phoneNumber,
      name: _self.profileForm.value.name
    }
    this.conf.postApiCall( 'brands/update-user/' + this.userId, data, function( res ){
      if (res.profileImage) {
        localStorage.setItem('profileImage', res.profileImage);
        _self.profileImage = res.profileImage;
        _self.profileForm.patchValue({imageName: res.profileImage});
        _self.profileForm.patchValue({imageFileName: res.profileImage});
      }
      _self.profileForm.patchValue(res);
      _self.conf.toast('Your details got updated successfully', '', 'success' );
      _self.conf.loader(2);
    },
    function( err ) {
      if ( err.detail != undefined ) {
        _self.conf.toast(err.detail, '', 'error' );
      } else {
        _self.conf.toast('', '', 'error' );
      }
      _self.conf.loader(2);
    });
  }
  uploadImage() {
    const file = this.image.nativeElement['files'][0];
    const reader = new FileReader();
    reader.addEventListener('loadend', () => {
      this.uploadProfilePicture(reader.result, file, file.name);
    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }
  uploadProfilePicture(blobResult, imageFile, fileName){
    this.conf.loader(1);
    let _self = this;
    console.log(imageFile, fileName);
    let formData = new FormData();
    formData.append('file', imageFile, fileName);
    this.conf.postApiCall( 'users/' + this.userId + '/profile', formData, function( res ){
      if (res.profileImage) {
        _self.profileForm.patchValue({imageName: fileName});
        _self.profileForm.patchValue({imageFileName: fileName});
        localStorage.setItem('profileImage', res.profileImage);
        _self.profileImage = res.profileImage;
        _self.conf.toast('Successfully updated your profile picture', '', 'success' );
        location.reload();
      } else {
        _self.conf.toast('Something wrong with the api', '', 'error' );
      }
      _self.conf.loader(2);
    },
    function( err ) {
      if ( err.detail != undefined ) {
        _self.conf.toast(err.detail, '', 'error' );
      } else {
        _self.conf.toast('', '', 'error' );
      }
      _self.conf.loader(2);
    });
  }
  changePassword() {
    this.conf.loader(1);
    let _self = this;
    if (this.profileForm.value.newpassword !== this.profileForm.value.confirmpassword) {
      _self.conf.toast('New password and confirm password do not match', '', 'warning' );
      return;
    }
    let data = {
      oldPassword: this.profileForm.value.oldpassword,
      newPassword: this.profileForm.value.newpassword,
    }
    this.conf.putApiCall( 'users/' + this.userId + '/change-password', data, function( res ){
      _self.conf.toast('Successfully updated your password!', '', 'success' );
      _self.profileForm.controls['oldpassword'].setValue('');
      _self.profileForm.controls['newpassword'].setValue('');
      _self.profileForm.controls['confirmpassword'].setValue('');
      _self.conf.loader(2);
    },
    function( err ) {
      _self.profileForm.controls['oldpassword'].setValue('');
      _self.profileForm.controls['newpassword'].setValue('');
      _self.profileForm.controls['confirmpassword'].setValue('');
      if ( err.detail != undefined ) {
        _self.conf.toast(err.detail, '', 'error' );
      } else {
        _self.conf.toast('Something went wrong. May be you can try using correct password.', '', 'error' );
      }
      _self.conf.loader(2);
    });
  }
}

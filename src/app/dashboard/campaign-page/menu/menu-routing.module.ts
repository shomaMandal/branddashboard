import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthguardGuard } from 'src/app/authguard.guard';
import { PageOneComponent } from './page-one/page-one.component';
import { PageTwoComponent } from './page-two/page-two.component';
import { PageThreeComponent } from './page-three/page-three.component';
import { PageFourComponent } from './page-four/page-four.component';
const route: Routes = [{
  path: 'user-profile',
  canActivate : [ AuthguardGuard ],
  component: PageOneComponent
}, {
  path: 'performance',
  canActivate : [ AuthguardGuard ],
  component: PageTwoComponent
}, {
  path: 'sample-content',
  canActivate : [ AuthguardGuard ],
  component: PageThreeComponent
}, {
  path: 'insight',
  canActivate : [ AuthguardGuard ],
  component: PageFourComponent
}];
@NgModule({
  imports: [RouterModule.forChild(route)],
  exports: [RouterModule]
})
export class MenuRoutingModule { }

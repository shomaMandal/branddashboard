import { NgModule } from '@angular/core';
import { CommonModule, TitleCasePipe } from '@angular/common';
import { MenuRoutingModule } from './menu-routing.module';
import { PageOneComponent } from './page-one/page-one.component';
import { PageTwoComponent } from './page-two/page-two.component';
import { PageThreeComponent } from './page-three/page-three.component';
import { PageFourComponent } from './page-four/page-four.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NGXChartsExtensionModule } from 'ngx-charts-extension';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
// import { MixChartComponent } from './mix-chart/mix-chart.component';
// import { ComboSeriesVerticalComponent } from './mix-chart/combo-series-vertical.component';
// import { ComboSeriesGroupVerticalComponent } from './mix-chart/combo-series-group-vertical.component';
export  function  HttpLoaderFactory(http:  HttpClient) {
  return  new  TranslateHttpLoader(http, 'assets/i18n/', '.json');
}
@NgModule({
  declarations: [
    PageOneComponent,
    PageTwoComponent,
    PageThreeComponent,
    PageFourComponent
  ],
  imports: [
    CommonModule,
    MenuRoutingModule,
    NgxChartsModule,
    NGXChartsExtensionModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    TranslateService
  ],
})
export class MenuModule { }

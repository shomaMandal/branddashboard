import { Component, OnInit } from '@angular/core';
import htmlToImage from 'html-to-image';
@Component({
  selector: 'app-page-four',
  templateUrl: './page-four.component.html',
  styleUrls: ['./page-four.component.scss']
})
export class PageFourComponent implements OnInit {
  cityTierData = [];
  bar = false;
  pie = false;
  colorScheme = {
    domain: ['#FF2744', '#FF2744', '#FF2744', '#FF2744', '#FF2744', '#FF2744']
  };
  list = [
    {type: 'single-selection',
     question: 'tap your question here?',
     respondentCount: 1000,
     pie:false,
     bar:false,
     data:[
      {
        "name": "Germany",
        "value": 8940000
      },
      {
        "name": "USA",
        "value": 5000000
      },
      {
        "name": "France",
        "value": 7200000
      },
        {
        "name": "UK",
        "value": 6200000
      }
     ],
     answer: [
      {no: 'a1',
      title: 'tap your question here',
      percent: '25.00%',
      count: '250'},
      {no: 'a1',
      title: 'tap your question here',
      percent: '25.00%',
      count: '250'},
      {no: 'a1',
      title: 'tap your question here',
      percent: '25.00%',
      count: '250'},
      {no: 'a1',
      title: 'tap your question here',
      percent: '25.00%',
      count: '250'},
    ]},
    {type: 'single-selection',
     question: 'tap your question here?',
     respondentCount: 1000,
     pie:false,
     bar:false,
     data:[
      {
        "name": "Germany",
        "value": 8940000
      },
      {
        "name": "USA",
        "value": 5000000
      },
      {
        "name": "France",
        "value": 7200000
      },
        {
        "name": "UK",
        "value": 6200000
      }
     ],
     answer: [
      {no: 'a1',
      title: 'tap your question here',
      percent: '25.00%',
      count: '250'},
      {no: 'a1',
      title: 'tap your question here',
      percent: '25.00%',
      count: '250'},
      {no: 'a1',
      title: 'tap your question here',
      percent: '25.00%',
      count: '250'},
      {no: 'a1',
      title: 'tap your question here',
      percent: '25.00%',
      count: '250'},
    ]},
    {type: 'single-selection',
     question: 'tap your question here?',
     respondentCount: 1000,
     pie:false,
     bar:false,
     data:[
      {
        "name": "Germany",
        "value": 8940000
      },
      {
        "name": "USA",
        "value": 5000000
      },
      {
        "name": "France",
        "value": 7200000
      },
        {
        "name": "UK",
        "value": 6200000
      }
     ],
     answer: [
      {no: 'a1',
      title: 'tap your question here',
      percent: '25.00%',
      count: '250'},
      {no: 'a1',
      title: 'tap your question here',
      percent: '25.00%',
      count: '250'},
      {no: 'a1',
      title: 'tap your question here',
      percent: '25.00%',
      count: '250'},
      {no: 'a1',
      title: 'tap your question here',
      percent: '25.00%',
      count: '250'},
    ]},
    {type: 'single-selection',
     question: 'tap your question here?',
     respondentCount: 1000,
     pie:false,
     bar:false,
     data:[
      {
        "name": "Germany",
        "value": 8940000
      },
      {
        "name": "USA",
        "value": 5000000
      },
      {
        "name": "France",
        "value": 7200000
      },
        {
        "name": "UK",
        "value": 6200000
      }
     ],
     answer: [
      {no: 'a1',
      title: 'tap your question here',
      percent: '25.00%',
      count: '250'},
      {no: 'a1',
      title: 'tap your question here',
      percent: '25.00%',
      count: '250'},
      {no: 'a1',
      title: 'tap your question here',
      percent: '25.00%',
      count: '250'},
      {no: 'a1',
      title: 'tap your question here',
      percent: '25.00%',
      count: '250'},
    ]},
  ];
  constructor() { }

  ngOnInit() {
    this.bar = true;
    this.cityTierData  = [
      {
        "name": "Germany",
        "value": 8940000
      },
      {
        "name": "USA",
        "value": 5000000
      },
      {
        "name": "France",
        "value": 7200000
      }
    ];
    let cityTierData = this.cityTierData;
    Object.assign(this, { cityTierData });
  }
  downloadGraph(canvasElement, type, chart){
    let node = document.getElementById(canvasElement);
    if (type == 'jpg') {
      node.style.backgroundColor = '#ffffff';
      htmlToImage.toJpeg(node, { quality: 0.95 }).then(function (dataUrl) {
        let link = document.createElement('a');
        link.download = canvasElement + chart + '.jpeg';
        link.href = dataUrl;
        link.click();
      }).catch(function (error) {
        console.error('oops, something went wrong!', error);
      });
    } else {
      node.style.backgroundColor = 'transparent';
      htmlToImage.toPng(node, { quality: 0.95 }).then(function (dataUrl) {
        let link = document.createElement('a');
        link.download = canvasElement + chart + '.png';
        link.href = dataUrl;
        link.click();
      }).catch(function (error) {
        console.error('oops, something went wrong!', error);
      });
    }
  }
}

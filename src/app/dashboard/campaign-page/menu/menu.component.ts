import { Component, OnInit } from '@angular/core';
import {Config} from '../../../../config';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  id;
  constructor(public conf: Config,
    public router: Router,
    public route: ActivatedRoute) {
      this.route.params.subscribe( params => {
        this.conf.translate.setDefaultLang(localStorage.getItem('lang'));
        this.conf.translate.use(localStorage.getItem('lang'));
        this.conf.navigate([localStorage.getItem('lang') + '/dashboard/' + localStorage.getItem('brandName') + '/badges/' + params['id1'] + '/campaign/' + params['id2'] + '/user-profile']);
      });
      // route.params.subscribe( params => {
        // this.conf.badgeId = params['id1'];
        // this.conf.campId = params['id2'];
        // this.conf.campId = params['id'];
        // this.conf.navigate([conf.lang + '/dashboard/badges/data/user-profile']);
      // });
  }
  ngOnInit() {}
}
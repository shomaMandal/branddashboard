import { Component, OnInit } from '@angular/core';
import { Config } from 'src/config';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { ServicesService } from 'src/app/services.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-page-three',
  templateUrl: './page-three.component.html',
  styleUrls: ['./page-three.component.scss']
})
export class PageThreeComponent implements OnInit {
  sampleList = [
    // {snsImage: 'https://peoplesmedicalcare.org/wp-content/uploads/2017/12/dummy-profile-img.jpg', title: 'weibo',
    //   data: [
    //     {content: ''},
    //     {content: ''},
    //     {content: ''},
    //     {content: ''}
    //   ]
    // },
    // {snsImage: 'https://peoplesmedicalcare.org/wp-content/uploads/2017/12/dummy-profile-img.jpg', title: 'wechat',
    //   data: [
    //     {content: ''},
    //     {content: ''},
    //     {content: ''},
    //     {content: ''}
    //   ]
    // }
  ];
  constructor(public conf: Config,
    public http: HttpClient,
    public translate: TranslateService,
    public user: ServicesService,
    public route: ActivatedRoute,
    public router: Router) {
      this.conf.translate.setDefaultLang('nl');
      this.conf.translate.use('nl');
      this.route.parent.params.subscribe( params => {
        this.conf.brandId = localStorage.getItem('brandId');
        this.conf.badgeId = params['id1'];
        this.conf.brandId = 2;
        this.conf.badgeId = 45;
        this.conf.campId = 6;
      });
    }

  ngOnInit() {
    //this.conf.loader(1);
    this.conf.lang = localStorage.getItem('lang');
    this.conf.translate.setDefaultLang(this.conf.lang);
    this.conf.translate.use(this.conf.lang);
    this.contentLinksForBadge();
  }
  contentLinksForBadge() {
    let _self = this;
    return new Promise((resolve, reject) => {
      this.http.get( this.conf.baseApiUrl + 'brand/' + this.conf.brandId + '/badge/' + this.conf.badgeId + '/contentLinksForBadge/?pageNo=0&size=25',
        {headers: { "Authorization": this.conf.getAuthToken() }, params: {}} )
      .toPromise()
      .then((res: any) => {
        _self.sampleList = res.content;
        console.log(res);
        // _self.result.push({"name": "Average household income", "value": Math.round(res)});
        resolve();
      }, err => {
        if (err.detail) {
          this.conf.toast(err.detail, '', 'error');
        } else {
          this.conf.toast('Something went wrong in income level distribution api', '', 'error');
        }
      });
    });
  }

}

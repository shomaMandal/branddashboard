import { Component, OnInit } from '@angular/core';
import { barChart, lineChartSeries } from '../mix-chart/data';
import { Config } from 'src/config';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { ServicesService } from 'src/app/services.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-page-two',
  templateUrl: './page-two.component.html',
  styleUrls: ['./page-two.component.scss']
})

export class PageTwoComponent implements OnInit {
  view: any[] = [700, 400];
  view1: any[] = [200, 100];
  colorScheme = {
    domain: ['#FF2744', '#FF2744', '#FF2744', '#FF2744', '#FF2744', '#FF2744']
  };
  multiColorScheme1 = {
    domain: ['#f5A0AD', '#FF74AE', '#FF2744']
  };
  multiColorScheme2 = {
    domain: ['#5DB7F7', '#14A934', '#8414A9']
  };
  cardColor: string = '#232837';
  // snsData = [
  //   {icon: 'https://peoplesmedicalcare.org/wp-content/uploads/2017/12/dummy-profile-img.jpg', title: 'weibo', dateRange: 'May 15th - June 12th', value: [
  //       {title: 'title1', count: '8958'},
  //       {title: 'title2', count: '8958'},
  //       {title: 'title3', count: '8958'}
  //     ]
  //   },
  //   {icon: 'https://peoplesmedicalcare.org/wp-content/uploads/2017/12/dummy-profile-img.jpg', title: 'wechat', dateRange: 'May 15th - June 12th', value: [
  //       {title: 'title1', count: '8958'},
  //       {title: 'title2', count: '8958'},
  //       {title: 'title3', count: '8958'}
  //     ]
  //   },
  //   {icon: 'https://peoplesmedicalcare.org/wp-content/uploads/2017/12/dummy-profile-img.jpg', title: 'sns1', dateRange: 'May 15th - June 12th', value: [
  //       {title: 'title1', count: '8958'},
  //       {title: 'title2', count: '8958'},
  //       {title: 'title3', count: '8958'}
  //     ]
  //   },
  //   {icon: 'https://peoplesmedicalcare.org/wp-content/uploads/2017/12/dummy-profile-img.jpg', title: 'sns2', dateRange: 'May 15th - June 12th', value: [
  //       {title: 'title1', count: '8958'},
  //       {title: 'title2', count: '8958'},
  //       {title: 'title3', count: '8958'}
  //     ]
  //   },
  //   {icon: 'https://peoplesmedicalcare.org/wp-content/uploads/2017/12/dummy-profile-img.jpg', title: 'sns3', dateRange: 'May 15th - June 12th', value: [
  //       {title: 'title1', count: '8958'},
  //       {title: 'title2', count: '8958'},
  //       {title: 'title3', count: '8958'}
  //     ]
  //   },
  //   {icon: 'https://peoplesmedicalcare.org/wp-content/uploads/2017/12/dummy-profile-img.jpg', title: 'sns4', dateRange: 'May 15th - June 12th', value: [
  //       {title: 'title1', count: '8958'},
  //       {title: 'title2', count: '8958'},
  //       {title: 'title3', count: '8958'}
  //     ]
  //   },
  //   {icon: 'https://peoplesmedicalcare.org/wp-content/uploads/2017/12/dummy-profile-img.jpg', title: 'sns5', dateRange: 'May 15th - June 12th', value: [
  //       {title: 'title1', count: '8958'},
  //       {title: 'title2', count: '8958'},
  //       {title: 'title3', count: '8958'}
  //     ]
  //   }
  // ];

  snsData: any;

  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  legendTitle = 'Legend';
  legendPosition = 'below';
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'GDP Per Capita';
  showGridLines = true;
  innerPadding = '10%';
  animations: boolean = true;
  barChart: any[] = barChart;
  lineChartSeries: any[] = lineChartSeries;
  lineChartScheme = {
    name: 'coolthree',
    selectable: true,
    group: 'Ordinal',
    domain: ['#01579b', '#7aa3e5', '#a8385d', '#00bfa5']
  };

  comboBarScheme = {
    name: 'singleLightBlue',
    selectable: true,
    group: 'Ordinal',
    domain: ['#01579b']
  };
  combo = false;
  showRightYAxisLabel: boolean = true;
  yAxisLabelRight: string = 'Utilization';

  result: any = [];
  resultMulti1: any = [];
  resultMulti2: any = [];
  result1: any = [];
  result2: any = [];
  result3: any = [];
  result4: any = [];
  resultLeft: any = [];
  resultRight: any = [];
  actionId = 207;

  snsIcon = [
    "Weibo_268x26-1545724835161.jpg",
    "-1545724860424.jpg",
    "-1545724873255.jpg",
    "-1545724891759.jpg",
    "-1545724920077.jpg",
    "230x0-1567963514017.jpg",
    "Meipa-1545724959860.png",
    "-1545724972500.jpg",
    "1-1545725010661.jpg",
    "1-1545725024820.jpg",
    "1-1545725042333.jpg",
    "1-1545725059814.jpg",
    "1-1545725119624.jpg",
    "Meitu_Log-154600233534-1548674936133.jpg"
  ]
  constructor(public conf: Config,
    public http: HttpClient,
    public translate: TranslateService,
    public user: ServicesService,
    public route: ActivatedRoute,
    public router: Router) {
      this.conf.translate.setDefaultLang('nl');
      this.conf.translate.use('nl');
      this.route.parent.params.subscribe( params => {
        // this.conf.brandId = localStorage.getItem('brandId');
        // this.conf.badgeId = params['id1'];
        this.conf.brandId = 2;
        this.conf.badgeId = 45;
        this.conf.campId = 6;
      });
    }

  ngOnInit() {
    //this.conf.loader(1);
    this.conf.lang = localStorage.getItem('lang');
    this.conf.translate.setDefaultLang(this.conf.lang);
    this.conf.translate.use(this.conf.lang);
    let p1 = this.numberOfParticipants();
    let p2 = this.averageIncome();
    let p3 = this.challengesPerformance();
    let p4 = this.attemptedPercentage();
    let p5 = this.dailyBadgePerformance();
    let p6 = this.dailyBadgeReviews();
    let p7 = this.weeklyBadgeOwners();
    let p8 = this.actionUrls();
    let p9 = this.collectChallengeUrls();
    let p10 = this.contentLinksForBadge();
    let p11 = this.exportContentLinks();
    // let p12 = this.calculateHashLinks();
    let p13 = this.questionInsights();
    let p14 = this.answersInsightPaginated();
    let p15 = this.markModified();
    Promise.all([p1, p2]).then(values => {
      let results = this.result;
      Object.assign(this, { results });
    });
    Promise.all([p3, p4]).then(values => {
      //this.conf.loader(2);  , 
      let result1 = this.result1;
      Object.assign(this, { result1 });
      let result2 = this.result2;
      Object.assign(this, { result2 });
      let result3 = this.result3;
      Object.assign(this, { result3 });
      let result4 = this.result4;
      Object.assign(this, { result4 });
    });
    Promise.all([p6, p7]).then(values => {
      let resultL = this.resultLeft;
      Object.assign(this, { resultL });
      let resultR = this.resultRight;
      Object.assign(this, { resultR });
    });
    Promise.all([p5]).then(values => {
      let multi = this.resultMulti1;
      let multiLine = this.resultMulti2;
      Object.assign(this, { multi });
      Object.assign(this, { multiLine });
    });
    //5, p8, p9, p10, p11, p12, p13, p14, p15
    Promise.all([p6, p7]).then(values => {
      let dataTraffic = [
        {
          "name": "Germany",
          "series": [
            {
              "name": "2010",
              "value": 7300000
            },
            {
              "name": "2011",
              "value": 8940000
            }
          ]
        },
        {
          "name": "USA",
          "series": [
            {
              "name": "2010",
              "value": 7870000
            },
            {
              "name": "2011",
              "value": 8270000
            }
          ]
        },
        {
          "name": "France",
          "series": [
            {
              "name": "2010",
              "value": 5000002
            },
            {
              "name": "2011",
              "value": 5800000
            }
          ]
        }
      ];
      Object.assign(this, { dataTraffic });
      setTimeout(() => {
        this.combo = true;
        Object.assign(this, { barChart });
        Object.assign(this, { lineChartSeries });
      }, 800);
    });
  }
  numberOfParticipants () {
    let _self = this;
    return new Promise((resolve, reject) => {
      this.http.get( this.conf.baseApiUrl + 'brandReports/brand/' + this.conf.brandId + '/campaign/' + this.conf.campId + '/brandMembersCount',
        {headers: { "Authorization": this.conf.getAuthToken() }, params: {}} )
      .toPromise()
      .then((res: any) => {
        _self.result.push({"name": "Total number", "value": res});
        resolve();
      }, err => {
        if (err.detail) {
          this.conf.toast(err.detail, '', 'error');
        } else {
          this.conf.toast('Something went wrong in income level distribution api', '', 'error');
        }
      });
    });
  }
  averageIncome () {
    let _self = this;
    return new Promise((resolve, reject) => {
      this.http.get( this.conf.baseApiUrl + 'brandReports/brand/' + this.conf.brandId + '/campaign/' + this.conf.campId + '/averageUserIncome?rebuild=false',
        {headers: { "Authorization": this.conf.getAuthToken() }, params: {}} )
      .toPromise()
      .then((res: any) => {
        // console.log(res);
        _self.result.push({"name": "Average household income", "value": Math.round(res)});
        resolve();
      }, err => {
        if (err.detail) {
          this.conf.toast(err.detail, '', 'error');
        } else {
          this.conf.toast('Something went wrong in income level distribution api', '', 'error');
        }
      });
    });
  }
  challengesPerformance() {
    let _self = this;
    return new Promise((resolve, reject) => {
      this.http.get( this.conf.baseApiUrl + 'brandReports/brand/' + 2 + '/badge/' + 45 + '/challengesPerformance?rebuild=true',
        {headers: { "Authorization": this.conf.getAuthToken() }, params: {}} )
      .toPromise()
      .then((res: any) => {
        // console.log(res);
        for (let index = 0; index < res.challengePerformanceReportDtos.length; index++) {
          res.challengePerformanceReportDtos[index].title = res.challengePerformanceReportDtos[index].snsName;
          res.challengePerformanceReportDtos[index].dateRange = "this is dummy Date";
          for (let _index = 0; _index < _self.snsIcon.length; _index++) {
            if (_index == res.challengePerformanceReportDtos[index].snsId) {
              res.challengePerformanceReportDtos[index].icon = _self.conf.putImage(_self.snsIcon[_index]);
            }
          }
          res.challengePerformanceReportDtos[index].value = [
            {title: "Challenges", count: res.challengePerformanceReportDtos[index].totalChallengesSubmitted},
            {title: "Interactions", count: res.challengePerformanceReportDtos[index].interactions},
            {title: "Reach", count: res.challengePerformanceReportDtos[index].reach}
          ];
        }
        _self.snsData = res.challengePerformanceReportDtos;
        console.log(_self.snsData);
        _self.result1 = [
          {
            "name": "Amount of action",
            "value": res.totalChallengesSubmitted
          }];
        _self.result2 = [
          {
            "name": "Interactive quantity",
            "value": res.totalInteraction
          }];
        _self.result3 = [
          {
            "name": "Exposure",
            "value": res.totalReach
          }];
        resolve();
      }, err => {
        if (err.detail) {
          this.conf.toast(err.detail, '', 'error');
        } else {
          this.conf.toast('Something went wrong in income level distribution api', '', 'error');
        }
      });
    });
  }
  attemptedPercentage () {
    let _self = this;
    return new Promise((resolve, reject) => {
      this.http.get( this.conf.baseApiUrl + 'brandReports/brand/' + this.conf.brandId + '/badge/' + this.conf.badgeId + '/attemptedPercentage?rebuild=false',
        {headers: { "Authorization": this.conf.getAuthToken() }, params: {}} )
      .toPromise()
      .then((res: any) => {
        // console.log(res);
        // _self.result.push({"name": "Average household income", "value": Math.round(res)});
        _self.result4 = [
          {
            "name": "User Engagement in %",
            "value": res.attemptedPercentage
          }];
        resolve();
      }, err => {
        if (err.detail) {
          this.conf.toast(err.detail, '', 'error');
        } else {
          this.conf.toast('Something went wrong in income level distribution api', '', 'error');
        }
      });
    });
  }
  dailyBadgePerformance() {
    let _self = this;
    return new Promise((resolve, reject) => {
      this.http.get( this.conf.baseApiUrl + 'brandReports/brand/' + 15 + '/badge/' + 106 + '/dailyBadgePerformance?rebuild=false',
        {headers: { "Authorization": this.conf.getAuthToken() }, params: {}} )
      .toPromise()
      .then((res: any) => {
        for (let index = 0; index < res.content.length; index++) {
          _self.resultMulti1.push({name: res.content[index].badgeId + index, series: [
            {"name": "interactionChange", "value": Number(res.content[index].interactionChange) + 15},
            {"name": "reachChange", "value": Number(res.content[index].reachChange) + 25},
            {"name": "challengesSubmittedChange", "value": Number(res.content[index].challengesSubmittedChange) + 35}
          ]});
        }
        console.log(_self.resultMulti1);
        for (let index = 0; index < res.content.length; index++) {
          _self.resultMulti2.push({name: res.content[index].badgeId + index, series: [
            {"name": "interaction", "value": Number(res.content[index].interactionChange) + index},
            {"name": "reach", "value": Number(res.content[index].reachChange) + index + 5},
            {"name": "challengesSubmitted", "value": Number(res.content[index].challengesSubmittedChange) + index + 8}
          ]});
        }
        console.log(_self.resultMulti2);
        resolve();
      }, err => {
        if (err.detail) {
          this.conf.toast(err.detail, '', 'error');
        } else {
          this.conf.toast('Something went wrong in income level distribution api', '', 'error');
        }
      });
    });
  }
  dailyBadgeReviews() {
    let _self = this;
    return new Promise((resolve, reject) => {
      this.http.get( this.conf.baseApiUrl + 'brandReports/brand/' + this.conf.brandId + '/badge/' + this.conf.badgeId + '/dailyBadgeReviews?rebuild=false&size=5',
        {headers: { "Authorization": this.conf.getAuthToken() }, params: {}} )
      .toPromise()
      .then((res: any) => {
        // console.log(res);
        // _self.result.push({"name": "Average household income", "value": Math.round(res)});
        let resData = [];
        for (let index = 0; index < res.content.length; index++) {
          let date = new Date(res.content[index].reviewDate);
          let formatted_date = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
          resData.push({"name": formatted_date, "value": Number(res.content[index].reviewCount)});
        }
        _self.resultLeft.push({name: "Germany", series: resData});
        resolve();
      }, err => {
        if (err.detail) {
          this.conf.toast(err.detail, '', 'error');
        } else {
          this.conf.toast('Something went wrong in income level distribution api', '', 'error');
        }
      });
    });
  }
  weeklyBadgeOwners() {
    let _self = this;
    return new Promise((resolve, reject) => {
      this.http.get( this.conf.baseApiUrl + 'brandReports/brand/' + this.conf.brandId + '/badge/' + this.conf.badgeId + '/weeklyBadgeOwners?rebuild=false',
        {headers: { "Authorization": this.conf.getAuthToken() }, params: {}} )
      .toPromise()
      .then((res: any) => {
        // console.log(res);
        let resData = [];
        for (let index = 0; index < res.length; index++) {
          resData.push({"name": res[index].weekNumber, "value": Number(res[index].userCount)});
        }
        _self.resultRight.push({name: "Germany", series: resData});
        resolve();
      }, err => {
        if (err.detail) {
          this.conf.toast(err.detail, '', 'error');
        } else {
          this.conf.toast('Something went wrong in income level distribution api', '', 'error');
        }
      });
    });
  }
  actionUrls() {
    let _self = this;
    return new Promise((resolve, reject) => {
      this.http.get( this.conf.baseApiUrl + 'collector/actionUrls',
        {headers: { "Authorization": this.conf.getAuthToken() }, params: {}} )
      .toPromise()
      .then((res: any) => {
        // console.log(res);
        // _self.result.push({"name": "Average household income", "value": Math.round(res)});
        resolve();
      }, err => {
        if (err.detail) {
          this.conf.toast(err.detail, '', 'error');
        } else {
          this.conf.toast('Something went wrong in income level distribution api', '', 'error');
        }
      });
    });
  }
  collectChallengeUrls() {
    let _self = this;
    return new Promise((resolve, reject) => {
      this.http.get( this.conf.baseApiUrl + 'collector/collectChallengeUrls/' + this.conf.badgeId,
        {headers: { "Authorization": this.conf.getAuthToken() }, params: {}} )
      .toPromise()
      .then((res: any) => {
        // console.log(res);
        // _self.result.push({"name": "Average household income", "value": Math.round(res)});
        resolve();
      }, err => {
        if (err.detail) {
          this.conf.toast(err.detail, '', 'error');
        } else {
          this.conf.toast('Something went wrong in income level distribution api', '', 'error');
        }
      });
    });
  }
  contentLinksForBadge() {
    let _self = this;
    return new Promise((resolve, reject) => {
      this.http.get( this.conf.baseApiUrl + 'brand/' + this.conf.brandId + '/badge/' + this.conf.badgeId + '/contentLinksForBadge/?pageNo=0&size=25',
        {headers: { "Authorization": this.conf.getAuthToken() }, params: {}} )
      .toPromise()
      .then((res: any) => {
        // console.log(res);
        // _self.result.push({"name": "Average household income", "value": Math.round(res)});
        resolve();
      }, err => {
        if (err.detail) {
          this.conf.toast(err.detail, '', 'error');
        } else {
          this.conf.toast('Something went wrong in income level distribution api', '', 'error');
        }
      });
    });
  }
  exportContentLinks() {
    let _self = this;
    return new Promise((resolve, reject) => {
      this.http.get( this.conf.baseApiUrl + 'brand/' + this.conf.brandId + '/challenge/40/exportContentLinks/',
        {headers: { "Authorization": this.conf.getAuthToken() }, params: {}} )
      .toPromise()
      .then((res: any) => {
        // console.log(res);
        // _self.result.push({"name": "Average household income", "value": Math.round(res)});
        resolve();
      }, err => {
        if (err.detail) {
          this.conf.toast(err.detail, '', 'error');
        } else {
          this.conf.toast('Something went wrong in income level distribution api', '', 'error');
        }
      });
    });
  }
  calculateHashLinks() {
    let _self = this;
    return new Promise((resolve, reject) => {
      this.http.get( this.conf.baseApiUrl + 'collector/calculateHashLinks',
        {headers: { "Authorization": this.conf.getAuthToken() }, params: {}} )
      .toPromise()
      .then((res: any) => {
        // console.log(res);
        // _self.result.push({"name": "Average household income", "value": Math.round(res)});
        resolve();
      }, err => {
        if (err.detail) {
          this.conf.toast(err.detail, '', 'error');
        } else {
          this.conf.toast('Something went wrong in income level distribution api', '', 'error');
        }
      });
    });
  }
  questionInsights() {
    let _self = this;
    return new Promise((resolve, reject) => {
      this.http.get( this.conf.baseApiUrl + 'brand/' + this.conf.brandId + '/badge/' + this.conf.badgeId + '/questionInsights?answerSize=10',
        {headers: { "Authorization": this.conf.getAuthToken() }, params: {}} )
      .toPromise()
      .then((res: any) => {
        // console.log(res);
        // _self.result.push({"name": "Average household income", "value": Math.round(res)});
        resolve();
      }, err => {
        if (err.detail) {
          this.conf.toast(err.detail, '', 'error');
        } else {
          this.conf.toast('Something went wrong in income level distribution api', '', 'error');
        }
      });
    });
  }
  answersInsightPaginated() {
    let _self = this;
    return new Promise((resolve, reject) => {
      this.http.get( this.conf.baseApiUrl + 'brand/' + this.conf.brandId + '/question/96/answersInsightPaginated?answerSize=10&pageNo=0',
        {headers: { "Authorization": this.conf.getAuthToken() }, params: {}} )
      .toPromise()
      .then((res: any) => {
        // console.log(res);
        // _self.result.push({"name": "Average household income", "value": Math.round(res)});
        resolve();
      }, err => {
        if (err.detail) {
          this.conf.toast(err.detail, '', 'error');
        } else {
          this.conf.toast('Something went wrong in income level distribution api', '', 'error');
        }
      });
    });
  }
  markModified() {
    let _self = this;
    return new Promise((resolve, reject) => {
      this.http.get( this.conf.baseApiUrl + 'collector/markModified/' + this.actionId,
        {headers: { "Authorization": this.conf.getAuthToken() }, params: {}} )
      .toPromise()
      .then((res: any) => {
        // console.log(res);
        // _self.result.push({"name": "Average household income", "value": Math.round(res)});
        resolve();
      }, err => {
        if (err.detail) {
          this.conf.toast(err.detail, '', 'error');
        } else {
          this.conf.toast('Something went wrong in income level distribution api', '', 'error');
        }
      });
    });
  }
}

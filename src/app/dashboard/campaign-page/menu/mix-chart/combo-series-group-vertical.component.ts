import { Component, Input, Output, EventEmitter, OnChanges, ChangeDetectionStrategy } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { __values, __decorate, __assign, __extends, __read, __spread } from 'tslib';
import { scaleTime, scaleLinear, scalePoint, scaleQuantile, scaleOrdinal, scaleBand } from 'd3-scale';
import { NgxChartsModule, BaseChartComponent, LineComponent, LineSeriesComponent,
  calculateViewDimensions, ViewDimensions, ColorHelper ,formatLabel, BarVertical2DComponent} from '@swimlane/ngx-charts';
@Component({
  selector: 'g[ngx-combo-charts-series-group-vertical]',
  template: `
    <svg:g ngx-charts-series-vertical *ngFor="let group of results; let index = index; trackBy: trackBy"
        [@animationState]="'active'"
        [attr.transform]="groupTransform(group)"
        [activeEntries]="activeEntries"
        [xScale]="innerScale"
        [yScale]="valueScale"
        [colors]="colors"
        [series]="group.series"
        [dims]="dims"
        [gradient]="gradient"
        [tooltipDisabled]="tooltipDisabled"
        [tooltipTemplate]="tooltipTemplate"
        [showDataLabel]="showDataLabel"
        [dataLabelFormatting]="dataLabelFormatting"
        [seriesName]="group.name"
        [roundEdges]="roundEdges"
        [animations]="animations"
        [noBarWhenZero]="noBarWhenZero"
        (select)="onClick($event, group)"
        (activate)="onActivate($event, group)"
        (deactivate)="onDeactivate($event, group)"
        (dataLabelHeightChanged)="onDataLabelMaxHeightChanged($event, index)">
    </svg:g>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('animationState', [
      transition('* => void', [
        style({
          opacity: 1,
          transform: '*',
        }),
        animate(500, style({opacity: 0, transform: 'scale(0)'}))
      ])
    ])
  ]
})
export class ComboSeriesGroupVerticalComponent  extends BaseChartComponent implements OnChanges   {
  xDomain: any;
  yDomain: any;
  transform: string;
  colorScheme = {
    domain: ['#5AA454', '#C7B42C', '#AAAAAA']
  };
  colorsLine: ColorHelper;
  xAxisHeight: number = 0;
  yAxisWidth: number = 0;
  legendOptions: any;
  xScaleLine;
  yScaleLine;
  xDomainLine;
  yDomainLine;
  seriesDomain;
  scaledAxis;
  combinedSeries;
  xSet;
  filteredDomain;
  hoveredVertical;
  yOrientLeft = 'left';
  yOrientRight = 'right';
  legendSpacing = 0;


  legend = false;
  legendTitle = 'Legend';
  legendPosition = 'right';
  scaleType = 'ordinal';
  showGridLines = true;
  trimXAxisTicks = true;
  trimYAxisTicks = true;
  rotateXAxisTicks = true;
  maxXAxisTickLength = 16;
  maxYAxisTickLength = 16;
  groupPadding = 16;
  barPadding = 8;
  roundDomains = false;
  roundEdges = true;
  showDataLabel = false;
  margin = [10, 20, 10, 20];
  dataLabelMaxHeight = { negative: 0, positive: 0 };
  @Input() dims: ViewDimensions;
  @Input() type = 'standard';
  @Input() series;
  @Input() seriesLine;
  @Input() xScale;
  @Input() yScale;
  @Input() colors: ColorHelper;
  @Input() tooltipDisabled: boolean = false;
  @Input() gradient: boolean;
  @Input() activeEntries: any[];
  @Input() seriesName: string;
  @Input() animations: boolean = true;
  @Input() noBarWhenZero: boolean = true;
  @Input() xAxis;
  @Input() yAxis;
  @Input() xScaleMax;
  @Input() yScaleMax;
  @Input() showXAxisLabel;
  @Input() showYAxisLabel;
  @Output() select = new EventEmitter();
  @Output() activate = new EventEmitter();
  @Output() deactivate = new EventEmitter();
  @Output() bandwidth = new EventEmitter();
  groupDomain;
  innerDomain;
  valuesDomain;
  groupScale;
  innerScale;
  valueScale;
  ngOnChanges(changes) {
    console.log("this is vertical 2d graph");
    
    this.update();
  }
  getXScale() {
    var spacing = this.groupDomain.length / (this.dims.width / this.barPadding + 1);
    return scaleBand()
        .rangeRound([0, this.dims.width])
        .paddingInner(spacing)
        .domain(this.groupDomain);
  }
  update() {
    super.update.call(this);

    let width;
    if (this.series.length) {
      width = this.xScale.bandwidth();
      this.bandwidth.emit(width);
    }
    if (!this.showDataLabel) {
      this.dataLabelMaxHeight = { negative: 0, positive: 0 };
    }
    this.margin = [10 + this.dataLabelMaxHeight.positive, 20, 10 + this.dataLabelMaxHeight.negative, 20];
    this.dims = calculateViewDimensions({
        width: this.width,
        height: this.height,
        margins: this.margin,
        showXAxis: this.xAxis,
        showYAxis: this.yAxis,
        xAxisHeight: this.xAxisHeight,
        yAxisWidth: this.yAxisWidth,
        showXLabel: this.showXAxisLabel,
        showYLabel: this.showYAxisLabel,
        showLegend: this.legend,
        legendType: this.schemeType,
        legendPosition: this.legendPosition
    });
    if (this.showDataLabel) {
        this.dims.height -= this.dataLabelMaxHeight.negative;
    }
    this.formatDates();
    this.groupDomain = this.getGroupDomain();
    this.innerDomain = this.getInnerDomain();
    this.valuesDomain = this.getValueDomain();
    this.groupScale = this.getGroupScale();
    this.innerScale = this.getInnerScale();
    this.valueScale = this.getValueScale();
    this.setColors();
    this.legendOptions = this.getLegendOptions();
    this.transform = "translate(" + this.dims.xOffset + " , " + (this.margin[0] + this.dataLabelMaxHeight.negative) + ")";
  }
  onDataLabelMaxHeightChanged(event, groupIndex) {
    var _this = this;
    if (event.size.negative) {
        this.dataLabelMaxHeight.negative = Math.max(this.dataLabelMaxHeight.negative, event.size.height);
    }
    else {
        this.dataLabelMaxHeight.positive = Math.max(this.dataLabelMaxHeight.positive, event.size.height);
    }
    if (groupIndex === this.results.length - 1) {
        setTimeout(function () { return _this.update(); });
    }
  }
  getGroupScale() {
    var spacing = this.groupDomain.length / (this.dims.height / this.groupPadding + 1);
    return scaleBand()
        .rangeRound([0, this.dims.width])
        .paddingInner(spacing)
        .paddingOuter(spacing / 2)
        .domain(this.groupDomain);
  }
  getInnerScale(){
    var width = this.groupScale.bandwidth();
    var spacing = this.innerDomain.length / (width / this.barPadding + 1);
    return scaleBand()
        .rangeRound([0, width])
        .paddingInner(spacing)
        .domain(this.innerDomain);
  }
  getValueScale() {
    var scale = scaleLinear()
        .range([this.dims.height, 0])
        .domain(this.valuesDomain);
    return this.roundDomains ? scale.nice() : scale;
  }
  getGroupDomain() {
    var e_1, _a;
    var domain = [];
    try {
        for (var _b = __values(this.results), _c = _b.next(); !_c.done; _c = _b.next()) {
            var group = _c.value;
            if (!domain.includes(group.label)) {
                domain.push(group.label);
            }
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
        }
        finally { if (e_1) throw e_1.error; }
    }
    return domain;
  }
  getInnerDomain = function () {
    var e_2, _a, e_3, _b;
    var domain = [];
    try {
        for (var _c = __values(this.results), _d = _c.next(); !_d.done; _d = _c.next()) {
            var group = _d.value;
            try {
                for (var _e = (e_3 = void 0, __values(group.series)), _f = _e.next(); !_f.done; _f = _e.next()) {
                    var d = _f.value;
                    if (!domain.includes(d.label)) {
                        domain.push(d.label);
                    }
                }
            }
            catch (e_3_1) { e_3 = { error: e_3_1 }; }
            finally {
                try {
                    if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
                }
                finally { if (e_3) throw e_3.error; }
            }
        }
    }
    catch (e_2_1) { e_2 = { error: e_2_1 }; }
    finally {
        try {
            if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
        }
        finally { if (e_2) throw e_2.error; }
    }
    return domain;
  }
  getValueDomain() {
    var e_4, _a, e_5, _b;
    var domain = [];
    try {
        for (var _c = __values(this.results), _d = _c.next(); !_d.done; _d = _c.next()) {
            var group = _d.value;
            try {
                for (var _e = (e_5 = void 0, __values(group.series)), _f = _e.next(); !_f.done; _f = _e.next()) {
                    var d = _f.value;
                    if (!domain.includes(d.value)) {
                        domain.push(d.value);
                    }
                }
            }
            catch (e_5_1) { e_5 = { error: e_5_1 }; }
            finally {
                try {
                    if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
                }
                finally { if (e_5) throw e_5.error; }
            }
        }
    }
    catch (e_4_1) { e_4 = { error: e_4_1 }; }
    finally {
        try {
            if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
        }
        finally { if (e_4) throw e_4.error; }
    }
    var min = Math.min.apply(Math, __spread([0], domain));
    var max = this.yScaleMax ? Math.max.apply(Math, __spread([this.yScaleMax], domain)) : Math.max.apply(Math, __spread([0], domain));
    return [min, max];
  }
  groupTransform(group) {
      return "translate(" + this.groupScale(group.label) + ", 0)";
  }
  onClick(data, group) {
      if (group) {
          data.series = group.name;
      }
      this.select.emit(data);
  }
  trackBy(index, item) {
      return item.name;
  }
  setColors() {
      var domain;
      if (this.schemeType === 'ordinal') {
          domain = this.innerDomain;
      }
      else {
          domain = this.valuesDomain;
      }
      this.colors = new ColorHelper(this.scheme, this.schemeType, domain, this.customColors);
  }
  getLegendOptions() {
      var opts = {
          scaleType: this.schemeType,
          colors: undefined,
          domain: [],
          title: undefined,
          position: this.legendPosition
      };
      if (opts.scaleType === 'ordinal') {
          opts.domain = this.innerDomain;
          opts.colors = this.colors;
          opts.title = this.legendTitle;
      }
      else {
          opts.domain = this.valuesDomain;
          opts.colors = this.colors.scale;
      }
      return opts;
  }
  updateYAxisWidth(_a) {
      var width = _a.width;
      this.yAxisWidth = width;
      this.update();
  }
  updateXAxisHeight(_a) {
      var height = _a.height;
      this.xAxisHeight = height;
      this.update();
  }
  onActivate(event, group, fromLegend) {
      if (fromLegend === void 0) { fromLegend = false; }
      var item = Object.assign({}, event);
      if (group) {
          item.series = group.name;
      }
      var items = this.results
          .map(function (g) { return g.series; })
          .flat()
          .filter(function (i) {
          if (fromLegend) {
              return i.label === item.name;
          }
          else {
              return i.name === item.name && i.series === item.series;
          }
      });
      this.activeEntries = __spread(items);
      this.activate.emit({ value: item, entries: this.activeEntries });
  }
  onDeactivate(event, group, fromLegend) {
      if (fromLegend === void 0) { fromLegend = false; }
      var item = Object.assign({}, event);
      if (group) {
          item.series = group.name;
      }
      this.activeEntries = this.activeEntries.filter(function (i) {
          if (fromLegend) {
              return i.label !== item.name;
          }
          else {
              return !(i.name === item.name && i.series === item.series);
          }
      });
      this.deactivate.emit({ value: item, entries: this.activeEntries });
  }
}

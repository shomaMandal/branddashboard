export let lineChartSeries = [
  {
    name: 'Tablets',
    series: [
      {
        name: 'USA',
        value: 50
      },
      {
        value: 80,
        name: 'United Kingdom'
      },
      {
        value: 85,
        name: 'France'
      },
      {
        value: 90,
        name: 'Japan'
      },
      {
        value: 100,
        name: 'China'
      }
    ]
  },
    {
    name: 'Cell Phones',
    series: [
      {
        value: 10,
        name: 'USA'
      },
      {
        value: 20,
        name: 'United Kingdom'
      },
      {
        value: 30,
        name: 'France'
      },
      {
        value: 40,
        name: 'Japan'
      },
      {
        value: 10,
        name: 'China'
      }
    ]
  },
    {
    name: 'Computers',
    series: [
      {
        value: 2,
        name: 'USA',
      },
      {
        value: 4,
        name: 'United Kingdom'
      },
      {
        value: 20,
        name: 'France'
      },
      {
        value: 30,
        name: 'Japan'
      },
      {
        value: 35,
        name: 'China'
      }
    ]
  }
];

  export let barChart: any = [
    {
      name: "USA",
      series: [
        {
          name: 'USA',
          value: 50000
        },
        {
          name: 'United Kingdom',
          value: 45000
        },
        {
          name: 'France',
          value: 44000
        }
      ]
    },
    {
      name: "United Kingdom",
      series: [
        {
          name: 'USA',
          value: 50000
        },
        {
          name: 'United Kingdom',
          value: 50000
        },
        {
          name: 'France',
          value: 50000
        }
      ]
    },
    {
      name: "France",
      series: [
        {
          name: 'USA',
          value: 50000
        },
        {
          name: 'United Kingdom',
          value: 50000
        },
        {
          name: 'France',
          value: 50000
        }
      ]
    },
    {
      name: "Japan",
      series: [
        {
          name: 'USA',
          value: 50000
        },
        {
          name: 'United Kingdom',
          value: 50000
        },
        {
          name: 'France',
          value: 50000
        }
      ]
    },
    {
      name: "China",
      series: [
        {
          name: 'USA',
          value: 50000
        },
        {
          name: 'United Kingdom',
          value: 50000
        },
        {
          name: 'France',
          value: 50000
        }
      ]
    }
  ];
  
import { Component, OnInit } from '@angular/core';
import { Config } from 'src/config';
import { HttpClient } from '@angular/common/http';
import { ServicesService } from 'src/app/services.service';

@Component({
  selector: 'app-campaign-page',
  templateUrl: './campaign-page.component.html',
  styleUrls: ['./campaign-page.component.scss']
})
export class CampaignPageComponent implements OnInit {
  badgeList = [];
  pageNo = 0;
  constructor(public conf: Config,
    public http: HttpClient,
    public user: ServicesService) {
      this.conf.translate.setDefaultLang('nl');
      this.conf.translate.use('nl');
  }
  ngOnInit() {
    this.getBadges();
  }
  getBadges() {
    this.conf.loader(1);
    const _self = this;
    this.conf.getApiCall( 'brands/badges/' + _self.pageNo + '?size=10', {}, function( res ) {
      if (res.numberOfElements) {
         _self.badgeList = res.content;
      }
      _self.conf.loader(2);
    }, function( err ) {
      if ( err != undefined ) {
        _self.conf.toast(err, '', 'error' );
      } else {
        _self.conf.toast('', '', 'error' );
      }
      _self.conf.loader(2);
    });
  }
  showReport(cmpId, badgeId){
    this.conf.campId = cmpId;
    this.conf.badgeId = badgeId;
    console.log(this.conf.campId);
    console.log(this.conf.badgeId);
    // this.conf.navigate(['/dashboard/badges/' + this.conf.badgeId + '/campaign/' + this.conf.campId + '/user-profile']);
    // this.conf.navigate(['/dashboard/'  + localStorage.getItem('brandName') +  '/badges/' + this.conf.badgeId + '/campaign/' + this.conf.campId + '/user-profile']);
    this.conf.navigate([this.conf.lang + '/dashboard/'  + localStorage.getItem('brandName') +  '/badges/' + this.conf.badgeId + '/campaign/' + this.conf.campId + '/user-profile']);
  }
}

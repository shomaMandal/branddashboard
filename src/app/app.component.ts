import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Config } from 'src/config';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'project-dashboard';
  constructor(public conf: Config) {}
}
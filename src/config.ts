import {Injectable} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {FormBuilder} from '@angular/forms';
import { TranslateService } from  '@ngx-translate/core';
import {TitleCasePipe} from '@angular/common';
import {environment} from './environments/environment';
import { ServicesService } from './app/services.service';

// declare var toastr:any;
// declare var $ :any;

@Injectable()
export class Config {
    environment = {
        production: false,
        backendUrl: "http://106.14.164.125:8082/",
        imgURL: "http://pjdaren.oss-cn-shanghai.aliyuncs.com/wom/prod/",
        exportUrl: "http://106.14.164.125:8082/"
    };
    brandId;
    lang = 'nl';
    campId;
    badgeId;
    declareWinner = false;
    invitemembers = false;
    showSurveyForm = false;
    termsAndConditions = "";
    targetProfileId = 0;
    brand;
    price;
    dueDate;
    campaignName = "";
    message1 = "";
    message2 = "";
    show = false;
    recordsFiltered = 0;
    private timedOutLoader: any;
    constructor(
        public router: Router,
        public translate:  TranslateService,
        public activeRoute: ActivatedRoute,
        public http: HttpClient,
        public titlecasePipe: TitleCasePipe,
        public formBuilder: FormBuilder,
        public service: ServicesService,
    ) {
    }
    baseIp = this.environment.backendUrl;
    tableDetail: any = {
        name: '',
        input: {}
    };
    baseApiUrl = this.baseIp + 'api/v1/';
    baseExportApiUrl = this.environment.exportUrl + 'api/v1/';

    globals = {
        apiBaseUrlPath: this.baseIp + 'api/v1/',
        imageBaseUrl : this.environment.imgURL,
    }
    loader( action ) {
        let loader = document.querySelector ('.loader-wrapper');
        if (action == 1) {
            loader.classList.add('show');
        }
        if (action == 2) {
            loader.classList.remove('show');
        }
    }

    canRun() {
        alert('canRun');
    }

    toast( msg, timeout, type ) {
        if (timeout == '') {
            timeout = 3000;
        }
        if (msg == '') {
            if (type == 'error' || type == 'warning') {
                msg = 'something went wrong';
            } else {
                msg = 'Successfully submitted';
            }
        }
        let container = document.querySelector ('.toast-wrapper');
        let div = this.service.renderer.createElement('div');
        div.innerHTML = msg;
        this.service.renderer.addClass(div, 'toast');
        this.service.renderer.setStyle(div, 'opacity', '0');
        this.service.renderer.setStyle(div, 'min-width', '400px');
        this.service.renderer.setStyle(div, 'max-width', '500px');
        this.service.renderer.setStyle(div, 'padding', '20px');
        this.service.renderer.setStyle(div, 'margin-bottom', '10px');
        this.service.renderer.setStyle(div, 'color', '#ffffff');
        this.service.renderer.setStyle(div, 'border-top-left-radius', '8px');
        this.service.renderer.setStyle(div, 'border-bottom-left-radius', '8px');
        this.service.renderer.setStyle(div, 'transition', 'opacity 0.5s');
        this.service.renderer.addClass(div, type);
        if (type == 'success') {
            this.service.renderer.setStyle(div, 'background-color', 'rgba(0, 128, 0, 0.6)');
        }
        if (type == 'error') {
            this.service.renderer.setStyle(div, 'background-color', 'rgba(255, 39, 68, 0.6)');
        }
        if (type == 'warning') {
            this.service.renderer.setStyle(div, 'background-color', 'rgba(255, 159, 39, 0.6)');
        }
        if (type == 'info') {
            this.service.renderer.setStyle(div, 'background-color', 'rgba(0, 93, 128, 0.6)');
        }
        container.appendChild(div);
        let s = this;
        setTimeout(() => {
            this.service.renderer.setStyle(div, 'transition', 'opacity 1s');
            this.service.renderer.setStyle(div, 'opacity', '1');
        }, 100);
        setTimeout(() => {
            s.service.renderer.setStyle(div, 'transition', 'opacity 1s');
            s.service.renderer.setStyle(div, 'opacity', '0');
            setTimeout(() => {
                this.service.renderer.removeChild(container, div);
            }, 1000);
        }, timeout);
    }
    currRoute() {
        return this.router.url.split('?')[0];
    }

    navigate( url , queryParams: object = {} ) {
        if ( queryParams ) {
            this.router.navigate( url , { queryParams: queryParams } );
        }
        this.router.navigate( url );
    }
    public httpApiGet( url, succCb, errCb ) {
        this.http.get( url ).subscribe(
            res => { succCb( res ); },
            err => { errCb( err); }
        );
    }
    public  httpPost( url, query, succCb, errCb ) {
        this.http.get( url, query ).subscribe(
            res => { succCb( res ); },
            err => { errCb( err); }
        );
    }
    public getApiCall( url, params, succCb, errCb ) {
        let mheaders = { "Authorization": this.getAuthToken() };
        this.http.get( this.baseApiUrl + url, {headers: mheaders, params:  params} ).subscribe(
            res => { succCb( res ); },
            err => { errCb( err ); }
        );
    }

    public getApiCallFullURL( url, params, succCb, errCb ) {
        let mheaders = { "Authorization": this.getAuthToken() };
        this.http.get( url, {headers: mheaders, params: params} ).subscribe(
            res => { succCb( res); },
            err => { errCb( err ); }
        );
    }

    public  putApiCall ( url, params, succCb, errCb ) {
        let mheaders = { "Authorization": this.getAuthToken() };
        this.http.put( this.baseApiUrl + url, params, {headers: mheaders, observe: 'response'} ).subscribe(
            res => { succCb( res ); },
            err => { errCb( err ); }
        );
    }
    public  putApiCallUser ( url, params, succCb, errCb ) {
        let mheaders = { "Authorization": this.getAuthToken(),"Content-Type": "application/json" };
        this.http.put( this.baseApiUrl + url, JSON.stringify(params), {headers: mheaders, observe: 'response'} ).subscribe(
            res => { succCb( res ); },
            err => { errCb( err ); }
        );
    }
    public deleteApiCall( url, mparams, succCb, errCb) {
        let mheaders = { "Authorization": this.getAuthToken() };
        this.http.request('delete', this.baseApiUrl + url, {headers: mheaders, params: mparams, observe: 'response',
        body: mparams} ).subscribe(
          (res) => { succCb( res ); },
            err => { errCb( err ); }
        );
    }
    public postApiCall( url, params, succCb, errCb ) {
        let mheaders = { "Authorization": this.getAuthToken() || "" };
        const fullUrl = this.baseApiUrl + url;
        this.http.post( fullUrl, params, {headers: mheaders}).subscribe(
            res => { succCb( res ); },
            err => {  errCb( err ); }
        );
    }
    public getAuthToken(){
        let token = localStorage.getItem( 'authToken' ) ;
        return token ? token : ""
    }
    putImage( imageName ){
        if (imageName == 'undefined') {
            imageName = 0;
        }
        return imageName ? this.globals.imageBaseUrl + imageName : '';
    }
    public checkBlankInput(text) { // need to optimize more
        if (text === '') {
            return true;
        }
        if (text != null) {
            if (text.replace(/\s+/g, '').length === 0) {
                text = '';
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
}